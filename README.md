To get application to work on your local machine, do the following:

1. Clone the repo (git clone riseagain@bitbucket.org:riseagain/balancepl.git).
2. Edit database.yml to match your PostgreSQL credentials.
3. Run rails db:setup && rails db:migrate