class RemoveAmountFromPayment < ActiveRecord::Migration[5.1]
  def change
    remove_column :payments, :amount, :float
  end
end
