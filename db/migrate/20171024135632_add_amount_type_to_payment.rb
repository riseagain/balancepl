class AddAmountTypeToPayment < ActiveRecord::Migration[5.1]
  def change
    add_column :payments, :amount_type, :integer, default: 0, index: true
  end
end
