class CreateInvestments < ActiveRecord::Migration[5.1]
  def change
    create_table :investments do |t|
      t.float :sum
      t.integer :period_in_months
      t.float :rate
      t.integer :time_months
      t.float :delay_rate

      t.timestamps
    end
  end
end
