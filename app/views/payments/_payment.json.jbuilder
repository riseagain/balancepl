json.extract! payment, :id, :amount_type, :investment_id, :created_at, :updated_at
json.url payment_url(payment, format: :json)
