json.extract! investment, :id, :sum, :period_in_months, :rate, :time_months, :delay_rate, :created_at, :updated_at
json.url investment_url(investment, format: :json)
