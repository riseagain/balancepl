class InvestmentsCalculator
  def initialize(investments)
    @investments = investments
  end

  def expected_rate
    (@investments.sum(&:rate) / @investments.count()).round(2)
  end

  def actual_rate
    (@investments.sum(&:actual_rate) / @investments.count).round(2)
  end  
end