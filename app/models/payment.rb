class Payment < ApplicationRecord
  validates :amount_type, presence: true
  default_scope { order(:id) }
  belongs_to :investment
  enum amount_type: { normal: 0, delayed: 1, early_full: 2, early_full_delay: 3 }

  def amount
    if amount_type == 'normal'
      sum = investment.monthly_payment_total
    elsif amount_type == 'delayed'
      sum = investment.monthly_payment_total_delay
    elsif amount_type == 'early_full'
      sum = investment.early_full_payment
    else
      sum = investment.early_full_payment_delay
    end

    sum.round(2)
  end
end
