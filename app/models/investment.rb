class Investment < ApplicationRecord
  validates :sum, presence: true, numericality: { greater_than: 0 }
  validates :period_in_months, presence: true, numericality: { greater_than: 0 }
  validates :rate, presence: true, numericality: { greater_than: 0, less_than: 1 }
  validates :time_months, presence: true, numericality: { greater_than: 0 }
  validates :delay_rate, presence: true, numericality: { greater_than: 0, less_than: 1 }

  default_scope { order(:id) }

  has_many :payments

  def expected_payed_rate
    (sum * rate / 12 * period_in_months).round(2)
  end

  def actually_payed_rate
    payments.sum do |payment|
      if ['normal', 'early_full'].include?(payment.amount_type)
        monthly_payment_rate
      else
        monthly_payment_rate_delay
      end
    end
  end

  def monthly_payment_debt
    (sum / period_in_months).round(2)
  end

  def monthly_payment_rate
    sum * rate / 12
  end

  def monthly_payment_total
    monthly_payment_debt + monthly_payment_rate
  end

  def monthly_payment_rate_delay
    sum * delay_rate / 12
  end

  def monthly_payment_total_delay
    monthly_payment_debt + monthly_payment_rate_delay
  end

  def early_full_payment
    sum - ((payments.count - 1) * monthly_payment_debt) + monthly_payment_rate
  end

  def early_full_payment_delay
    sum - (payments.count * monthly_payment_debt) + monthly_payment_rate_delay
  end  

  def actual_rate
    (actually_payed_rate / sum / period_in_months * 12).round(2)
  end
end