class CalculationsController < ApplicationController
  def index
  end

  def calculate_rates
    calculator = InvestmentsCalculator.new(Investment.all)
    @actual_rate = calculator.actual_rate
    @expected_rate = calculator.expected_rate
  end
end
