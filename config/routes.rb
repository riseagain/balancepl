Rails.application.routes.draw do
  resources :payments
  resources :investments
  root 'calculations#index'
  post 'calculations/calculate_rates', as: 'calculate_rates'
end
